#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <iostream>
#include <fstream>
#include <algorithm>

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Polyhedron_3<Kernel> Polyhedron;
typedef Polyhedron::Facet_iterator Facet_iterator;
typedef Polyhedron::Vertex_iterator Vertex_iterator;
typedef Polyhedron::Halfedge_iterator Halfedge_iterator;
typedef Polyhedron::Halfedge_around_facet_circulator Halfedge_facet_circulator;
typedef CGAL::Point_3<CGAL::Epick> Point_3;
typedef std::map<Polyhedron::Vertex_handle, int> Facet_int_map;

int PROFONDEUR_MAX = 3;
int NBSOMMET_MAX = 10;

Facet_int_map association;

class Noeud {
    float xmin;
    float xmax;
    float ymin;
    float ymax;
    float zmin;
    float zmax;

    int profondeur;
    int colorSeed;
    static int nbNoeud;

    std::list<Noeud> fils;
    std::list<Polyhedron::Vertex_handle> sommets;

    public:

    Noeud(): xmin(0.0), xmax(0.0), ymin(0.0), ymax(0.0), zmin(0.0), zmax(0.0), profondeur(0),colorSeed(nbNoeud){nbNoeud++; std::cout<<"nouveau noeud"<<std::endl;};
    Noeud(float xmi,float xma,float ymi,float yma,float zmi,float zma,int prof):xmin(xmi), xmax(xma), ymin(ymi), ymax(yma), zmin(zmi), zmax(zma), profondeur(++prof),colorSeed(nbNoeud){
        nbNoeud++;
    };
    Noeud(const  Noeud & n){
        xmin = n.getXMin();
        xmax = n.getXMax();
        ymin = n.getYMin();
        ymax = n.getYMax();
        zmin = n.getZMin();
        zmax = n.getZMax();
        profondeur = n.getProfondeur();
        colorSeed = n.colorSeed;
    };

    std::list<Noeud> & getFils()
    {
        return fils;
    }

    std::list<Polyhedron::Vertex_handle> & getSommets()
    {
        return sommets;
    }


    float getXMin() const
    {
        return xmin;
    }

    float getXMax() const
    {
        return xmax;
    }

    float getYMin() const
    {
        return ymin;
    }

    float getYMax() const
    {
        return ymax;
    }

    float getZMin() const
    {
        return zmin;
    }

    float getZMax() const
    {
        return zmax;
    }

    int getProfondeur() const
    {
        return profondeur;
    }

    int getColorSeed() const
    {
        return colorSeed;
    }

    void ajoutSommet(Polyhedron::Vertex_handle &somm){
        if(fils.empty() && (sommets.size() < NBSOMMET_MAX || profondeur == PROFONDEUR_MAX)){
            sommets.push_back(somm);
            association[somm] = colorSeed ;
        }
        else
        {
            if (fils.empty()){
                Creer_fils();
                std::list<Polyhedron::Vertex_handle>::iterator it = sommets.begin();

                    for(it;it!=sommets.end();it++)
                    {
                        trouver_noeud(*it).ajoutSommet(*it);  
                    }
                    sommets.clear();
            }
            trouver_noeud(somm).ajoutSommet(somm);
        }

    }

    void Creer_fils()
        {
            float xinte = (xmax+xmin) / 2;
            float yinte = (ymax+ymin) / 2;
            float zinte = (zmax+zmin) / 2;

            Noeud number1(xmin, xinte, ymin, yinte, zmin, zinte, profondeur);
            Noeud number2(xinte, xmax, ymin, yinte, zmin, zinte, profondeur); 
            Noeud number3(xmin, xinte, yinte, ymax, zmin, zinte, profondeur); 
            Noeud number4(xinte, xmax, yinte, ymax, zmin, zinte, profondeur); 
            Noeud number5(xmin, xinte, ymin, yinte, zinte, zmax, profondeur); 
            Noeud number6(xinte, xmax, ymin, yinte, zinte, zmax, profondeur); 
            Noeud number7(xmin, xinte, yinte, ymax, zinte, zmax, profondeur); 
            Noeud number8(xinte, xmax, yinte, ymax, zinte, zmax, profondeur);

            fils.push_back(number1);
            fils.push_back(number5);
            fils.push_back(number3);
            fils.push_back(number7);
            fils.push_back(number2);
            fils.push_back(number6);
            fils.push_back(number4);
            fils.push_back(number8);
        }

    Noeud& trouver_noeud(Polyhedron::Vertex_handle &pt){
            int x;
            int y;
            int z;
            float xinte = (xmax+xmin) / 2;
            float yinte = (ymax+ymin) / 2;
            float zinte = (zmax+zmin) / 2;

            if(pt->point().x() < xinte)
            {
                x = 0;
            }
            else
            {
                x = 1;
            }
            if(pt->point().y() < yinte)
            {
                y = 0;
            }
            else
            {
                y = 1;
            }
            if(pt->point().z() < zinte)
            {
                z = 0;
            }
            else
            {
                z = 1;
            }

            std::list<Noeud>::iterator it = fils.begin();

            int index = 4*x + 2*y +z;

            for(int i = 0;i<index;i++)
            {
                it++;
            }

            return *it;
        }
};

int Noeud::nbNoeud = 1; //init attribut de classe

Noeud creer_octree(Polyhedron & mesh){
    float xmin = 0.0;
    float ymin = 0.0;
    float zmin = 0.0;
    float xmax = 0.0;
    float ymax = 0.0;
    float zmax = 0.0;

    //RECHERCHE DU MIN ET DU MAX SUR LES AXES
    for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i) {
		float x = i->point().x();
        float y = i->point().y();
        float z = i->point().z();
        if(x < xmin){
            xmin = x;
        }
        else if(x > xmax){
            xmax = x;
        }

        if(y < ymin){
            ymin = y;
        }
        else if(y > ymax){
            ymax = y;
        }

        if(z < zmin){
            zmin = z;
        }
        else if(z > zmax){
            zmax = z;
        }
	}

    Noeud root(xmin, xmax, ymin, ymax, zmin, zmax,0);

    for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i){
        root.ajoutSommet(i);
	}

    return root;
}

void initMap(Polyhedron & mesh){
    for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i) {
        association.insert(std::pair<Polyhedron::Vertex_handle, int>(i,0));
    }
}

void sauverSegmentation(std::string & filename, Polyhedron & mesh, Facet_int_map & valeurs){
    std::ofstream myfile;
	myfile.open(filename);
    //ENTETE
    CGAL::set_ascii_mode( myfile);
    myfile << "COFF" << std::endl << mesh.size_of_vertices() << ' '
              << mesh.size_of_facets() << ' ' << mesh.size_of_halfedges()/2 << std::endl;
    for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i) {
        srand(association[i]);
        myfile << i->point();
        myfile << ' ' << rand()%255;
        myfile << ' ' << rand()%255;
        myfile << ' ' << rand()%255;
        myfile << ' ' << "250" << std::endl;
	}

    // PARCOUR DES FACES
    for (  Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
        Halfedge_facet_circulator j = i->facet_begin();
        CGAL_assertion( CGAL::circulator_size(j) >= 3);
        myfile << CGAL::circulator_size(j) << ' ';
        do {
			myfile << ' ' << std::distance(mesh.vertices_begin(), j->vertex());
        } while ( ++j != i->facet_begin());
        //APPLICATION COULEUR
        myfile << std::endl;
    }

	myfile.close();
}

struct Fusionsommets {
    float xmoy = 0;
    float ymoy = 0;
    float zmoy = 0;

};

int num = 0;

void FusionPts(Noeud &origine, std::vector<Fusionsommets> &v_fusion, std::map<Polyhedron::Vertex_handle,int> &asso){
    if(!origine.getFils().empty()){
        for(std::list<Noeud>::iterator it = origine.getFils().begin();it!= origine.getFils().end();it++){
            FusionPts(*it,v_fusion,asso);
        }
    }
    else if(origine.getSommets().size()>0){
        Fusionsommets fus;
        for(std::list<Polyhedron::Vertex_handle>::iterator it = origine.getSommets().begin();it!= origine.getSommets().end();it++){
            fus.xmoy += (*it)->point().x();
            fus.ymoy += (*it)->point().y();
            fus.zmoy += (*it)->point().z();
            asso.insert(std::pair<Polyhedron::Vertex_handle,int>(*it,num));
        }
        fus.xmoy /= origine.getSommets().size();
        fus.ymoy /= origine.getSommets().size();
        fus.zmoy /= origine.getSommets().size();
        ++num;
        v_fusion.push_back(fus);
    }
}

void sauverSimplification(std::string & filename, Polyhedron & mesh, Noeud &origine){

    std::vector<Fusionsommets> fusions;
    std::map<Polyhedron::Vertex_handle,int> asso_sommets;
    FusionPts(origine,fusions,asso_sommets);

    int nb_faces = 0;
    std::ofstream myfile;
	myfile.open(filename);
    //ENTETE
    CGAL::set_ascii_mode( myfile);
    myfile << "OFF" << std::endl << fusions.size() << ' '
              <<"        " << ' ' << "        " << std::endl;

    for (std::vector<Fusionsommets>::iterator it = fusions.begin(); it != fusions.end(); ++it) {
		myfile << (*it).xmoy << " " << (*it).ymoy << " " << (*it).zmoy << std::endl;
	}

    // PARCOUR DES FACES
    for (  Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
        Halfedge_facet_circulator j = i->facet_begin();
        CGAL_assertion( CGAL::circulator_size(j) >= 3);

        int indice1 = asso_sommets.find(j->vertex())->second;
        j++;
        int indice2 = asso_sommets.find(j->vertex())->second;
        j++;
        int indice3 = asso_sommets.find(j->vertex())->second;

        if(indice1 != indice2 && indice1 != indice3 && indice2 != indice3) {
            myfile << "3 " << indice1 << " " << indice2 << " " << indice3 << std::endl;
            nb_faces++;
        }
    }
    myfile.seekp(0); // retour au debut du fichier
    myfile << "OFF" << std::endl << fusions.size() << " " << nb_faces << ' ' << nb_faces*3;
    myfile.close();
}

int main(int argc, char* argv[])
{
    std::string filename = "TP5_test.off";
    std::string filename2 = "TP5_SIMPLI_test.off";
    if (argc < 2) {
		std::cerr << "Il manque un paramètre au programme. Veuillez lui donner en entrée un nom de fichier au format off." << std::endl;
		return 1;
	}
	
	Polyhedron mesh;
    std::ifstream input(argv[1]);
	if (!input || !(input >> mesh) || mesh.is_empty()) {
        std::cerr << "Le fichier donné n'est pas un fichier off valide." << std::endl;
        return 1;
    }
    std::cout << "ARGC : " << argc << std::endl;
    if ((argc == 4))  {
        PROFONDEUR_MAX = std::stoi((std::string)argv[2],nullptr,0);
        NBSOMMET_MAX = std::stoi((std::string)argv[3],nullptr,0);
        std::cout << "PROFONDEUR MAX : " << PROFONDEUR_MAX << " NBSOMMET_MAX : " << NBSOMMET_MAX << std::endl;
    }

    initMap(mesh);
    Noeud arbre = creer_octree(mesh);
    sauverSegmentation(filename,mesh,association);
    sauverSimplification(filename2,mesh,arbre);

    return 0;
}