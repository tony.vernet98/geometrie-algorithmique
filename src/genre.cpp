#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <iostream>
#include <fstream>
typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Polyhedron_3<Kernel> Polyhedron;
typedef Polyhedron::Facet_iterator Facet_iterator;
typedef Polyhedron::Vertex_iterator Vertex_iterator;
typedef Polyhedron::Halfedge_iterator Halfedge_iterator;
typedef Polyhedron::Halfedge_around_facet_circulator Halfedge_facet_circulator;
typedef CGAL::Point_3<CGAL::Epick> Point_3;



int main(int argc, char* argv[])
{
	if (argc < 2) {
		std::cerr << "Il manque un paramètre au programme. Veuillez lui donner en entrée un nom de fichier au format off." << std::endl;
		return 1;
	}
	
	Polyhedron mesh;
  std::ifstream input(argv[1]);
	 if (!input || !(input >> mesh) || mesh.is_empty()) {
    std::cerr << "Le fichier donné n'est pas un fichier off valide." << std::endl;
    return 1;
  }
  
  unsigned int nbVerts = 0;
	for (Vertex_iterator i = mesh.vertices_begin(); i != mesh.vertices_end(); ++i) {
		++nbVerts;
	}
	std::cout << "Nombre de sommets: " << nbVerts << std::endl;
	
	unsigned int nbEdges = 0;
	for (Halfedge_iterator i = mesh.halfedges_begin(); i != mesh.halfedges_end(); ++i) {
		++nbEdges;
	}
	nbEdges /= 2;
	std::cout << "Nombre d'arêtes: " << nbEdges << std::endl;

	unsigned int nbFaces = 0;
	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
		++nbFaces;
	}
	std::cout << "Nombre de faces: " << nbFaces << std::endl;
	
	unsigned int euler = nbVerts - nbEdges + nbFaces;
	unsigned int genus = (2 - euler) / 2;
	std::cout << "En supposant que le maillage contienne une unique surface sans bord, alors son genre est de " << genus << std::endl;

	double perimetre;

	//map de la face avec son perimetre
	for (  Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
		perimetre = 0.0;
        Halfedge_facet_circulator j = i->facet_begin();
        CGAL_assertion( CGAL::circulator_size(j) >= 3);
        do {
            perimetre += CGAL::squared_distance(j->vertex()->point(),j->opposite()->vertex()->point());
        } while ( ++j != i->facet_begin());
		std::cout << "perimetre de la face :" << perimetre << std::endl;
        std::cout << std::endl;
    }
	
	//Ecriture du fichier
	// Write polyhedron in Object File Format (OFF).
	std::ofstream myfile;
	myfile.open("/home/eolion/Geometrie_algo/tp1-geoalgo-zz3f1/data/cubeColor.off");


    CGAL::set_ascii_mode( myfile);
    myfile << "OFF" << std::endl << mesh.size_of_vertices() << ' '
              << mesh.size_of_facets() << ' ' << mesh.size_of_halfedges()/2 << std::endl;
    std::copy( mesh.points_begin(), mesh.points_end(),
               std::ostream_iterator<Point_3>( myfile, "\n"));
    for (  Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
        Halfedge_facet_circulator j = i->facet_begin();
        // Facets in polyhedral surfaces are at least triangles.
        CGAL_assertion( CGAL::circulator_size(j) >= 3);
        myfile << CGAL::circulator_size(j) << ' ';
        do {
			myfile << ' ' << std::distance(mesh.vertices_begin(), j->vertex());
        } while ( ++j != i->facet_begin());
        myfile << std::endl;
    }

	myfile.close();
    return 0;
}
