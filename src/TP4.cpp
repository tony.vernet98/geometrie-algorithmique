#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <iostream>
#include <fstream>
#include <algorithm>

typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
typedef CGAL::Polyhedron_3<Kernel> Polyhedron;
typedef std::map<Polyhedron::Facet_handle, double> Facet_double_map;
typedef std::map<Polyhedron::Facet_handle, int> Facet_int_map;
typedef Polyhedron::Facet_iterator Facet_iterator;
typedef Polyhedron::Vertex_iterator Vertex_iterator;
typedef Polyhedron::Halfedge_iterator Halfedge_iterator;
typedef Polyhedron::Halfedge_around_facet_circulator Halfedge_facet_circulator;
typedef CGAL::Point_3<CGAL::Epick> Point_3;


Facet_int_map seuillageSimple(Polyhedron & mesh, Facet_double_map & valeurs){
    double moyenne = 0.0;
    Facet_int_map result;

    //MOYENNE
    std::for_each(valeurs.begin(),valeurs.end(),[&moyenne](std::pair<Polyhedron::Facet_handle, double> val){moyenne += val.second;});
    moyenne = moyenne/valeurs.size();

    //SEUILLAGE
    std::for_each(valeurs.begin(),valeurs.end(),[moyenne,&result](std::pair<Polyhedron::Facet_handle, double> val){
        if(val.second<moyenne)
            result.insert(std::pair<Polyhedron::Facet_handle, int>(val.first,1));
        else 
            result.insert(std::pair<Polyhedron::Facet_handle, int>(val.first,2));
            });
    return result;
}

void sauverSegmentation(std::string & filename, Polyhedron & mesh, Facet_int_map & valeurs){
    std::ofstream myfile;
	myfile.open(filename);
    //ENTETE
    CGAL::set_ascii_mode( myfile);
    myfile << "OFF" << std::endl << mesh.size_of_vertices() << ' '
              << mesh.size_of_facets() << ' ' << mesh.size_of_halfedges()/2 << std::endl;
    std::copy( mesh.points_begin(), mesh.points_end(),
               std::ostream_iterator<Point_3>( myfile, "\n"));

    // PARCOUR DES FACES
    for (  Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
        Halfedge_facet_circulator j = i->facet_begin();
        CGAL_assertion( CGAL::circulator_size(j) >= 3);
        myfile << CGAL::circulator_size(j) << ' ';
        do {
			myfile << ' ' << std::distance(mesh.vertices_begin(), j->vertex());
        } while ( ++j != i->facet_begin());
        //APPLICATION COULEUR
        srand(valeurs[i]);
        myfile << ' ' << rand()%255;
        myfile << ' ' << rand()%255;
        myfile << ' ' << rand()%255;
        myfile << std::endl;
    }

	myfile.close();
}

Facet_double_map CalculPerimetre(Polyhedron & mesh){
    //CALCUL PERIMETRE
    double perimetre;
    Facet_double_map result;
    for (  Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
        perimetre = 0.0;
        Halfedge_facet_circulator j = i->facet_begin();
        CGAL_assertion( CGAL::circulator_size(j) >= 3);
        do {
            perimetre += CGAL::squared_distance(j->vertex()->point(),j->opposite()->vertex()->point());
        } while ( ++j != i->facet_begin());
        result.insert(std::pair<Polyhedron::Facet_handle, double>(i,perimetre));
    }

    return result;
}

Facet_double_map CalculAire(Polyhedron& mesh) {
    //CALCUL AIRE
    double aire;
    Facet_double_map result;
    for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
        aire = 0.0;
        Halfedge_facet_circulator j = i->facet_begin();
        CGAL_assertion(CGAL::circulator_size(j) >= 3);

        do {
            if (i->is_triangle())
            {
                aire = CGAL::squared_area(
                    j->vertex()->point(),
                    j->next()->vertex()->point(),
                    j->opposite()->vertex()->point()) / 2;
            }
            // DECOMPOSITION EN 2 TRIANGLES
            else if (i->is_quad())
            {
                aire = CGAL::squared_area(
                    j->vertex()->point(),
                    j->next()->vertex()->point(),
                    j->opposite()->vertex()->point()) / 2;
                aire += CGAL::squared_area(
                    j->next()->next()->vertex()->point(),
                    j->next()->vertex()->point(),
                    j->opposite()->vertex()->point()) / 2;
            }
        } while (++j != i->facet_begin());

        result.insert(std::pair<Polyhedron::Facet_handle, double>(i, aire));
    }

    return result;
}

Facet_int_map parcourRecursif(Facet_int_map & segmentationOrigine, Facet_int_map & segmentationParCC, int classe, Facet_iterator i){
    if(segmentationParCC[i] == -1){
        Halfedge_facet_circulator j = i->facet_begin();
        segmentationParCC[i] = classe;
        do {
            if (segmentationOrigine[j->opposite()->facet()] == segmentationOrigine[i]){
                segmentationParCC = parcourRecursif(segmentationOrigine,segmentationParCC,classe,j->opposite()->facet());
            }
        } while ( ++j != i->facet_begin());
    }
    return segmentationParCC;
}

Facet_int_map segmentationParCC(Polyhedron & mesh, Facet_int_map & segmentation){
    
    Facet_int_map segmentationCC;
    for (  Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
        // -1 = FACE NON VISITEE
        segmentationCC.insert(std::pair<Polyhedron::Facet_handle, int>(i,-1));
    }

    int classe =  1; //PAS DE CLASSE 0 POUR NE PAS AVOIR D'ERREUR SUR LA COULEUR
    for (  Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i) {
        segmentationCC = parcourRecursif(segmentation, segmentationCC, classe,i);
        classe++;
    }

    return segmentationCC;
}

Facet_int_map seuillageOtsu(Polyhedron & mesh, Facet_double_map & valeurs){

    Facet_int_map result;

    //RECUPERATION DE LA VALEUR MAX
    auto max = std::max_element(valeurs.begin(), valeurs.end(),
    [](const std::pair<Polyhedron::Facet_handle, double>& p1, const std::pair<Polyhedron::Facet_handle, double>& p2) {
        return p1.second < p2.second; });

    //RECUPERATION DE LA VALEUR MIN
    auto min = std::min_element(valeurs.begin(), valeurs.end(),
    [](const std::pair<Polyhedron::Facet_handle, double>& p1,const std::pair<Polyhedron::Facet_handle, double>& p2) {
        return p1.second < p2.second; });

    std::cout << "max : " << max->second << "min : " << min->second << std::endl;

    double ecart = (max->second - min->second) / 63.0 ; // 63 permet d'inclure la valeur max dans une cellule du tableau de 64
    
    int total = valeurs.size();

    //CONSTRUCTION HISTOGRAMME
    double histogramme[64] ;

    for(int i = 0 ; i<64;i++){
        histogramme[i] = 0;
    }

    for(int i=0;i<64;i++){
        std::for_each(valeurs.begin(),valeurs.end(),[&histogramme,&i,&ecart,min,total](std::pair<Polyhedron::Facet_handle, double> val){
        if ((val.second >= ((double)i*ecart + min->second)) && (val.second < ((double)(i+1)*ecart+ min->second))){
            histogramme[i] += 1.0/total;
        }
            });
    }

    //AFFICHAGE DE L'HISTOGRAMME
    std::cout << "H : ";
    for(int i = 0 ; i<64;i++){
        std::cout << histogramme[i] << " ";
    }
    std::cout << std::endl;

    //METHODE OTSU
    double probabilite[64], moyenne[64];
    double max_bt, bt[64];
    int seuil;


    for(int i = 0; i < 64; i++) {
        probabilite[i] = 0.0;
        moyenne[i] = 0.0;
        bt[i] = 0.0;
    }

    probabilite[0] = histogramme[0];

    for(int i = 1; i < 64; i++) {
        probabilite[i] = probabilite[i - 1] + histogramme[i];
        moyenne[i] = moyenne[i - 1] + i * histogramme[i];
    }

    seuil = 0;
    max_bt = 0.0;

    for(int i = 0; i < 64; i++) {
        if(probabilite[i] != 0.0 && probabilite[i] != 1.0)
            bt[i] = pow(moyenne[i] * probabilite[i] - moyenne[i], 2) / (probabilite[i] * (1.0 - probabilite[i]));
    else
        bt[i] = 0.0;
        if(bt[i] > max_bt) {
            max_bt = bt[i];
            seuil = i;
        }
    }

    std::cout << "meilleur separateur : " << seuil << std::endl;

    double separateur = seuil * ecart + min->second;

    //APLICATION DU SEUIL SUR LE MAILLAGE
    std::for_each(valeurs.begin(),valeurs.end(),[separateur,&result](std::pair<Polyhedron::Facet_handle, double> val){
        if(val.second<=separateur)
            result.insert(std::pair<Polyhedron::Facet_handle, int>(val.first,3));
        else 
            result.insert(std::pair<Polyhedron::Facet_handle, int>(val.first,2));
            });

    return result;

}

int main(int argc, char* argv[])
{
    Facet_double_map faces;
    Facet_int_map faces_seuillage;
    std::string filename = "TP4_test.off";
    if (argc < 2) {
		std::cerr << "Il manque un paramètre au programme. Veuillez lui donner en entrée un nom de fichier au format off." << std::endl;
		return 1;
	}
	
	Polyhedron mesh;
    std::ifstream input(argv[1]);
	if (!input || !(input >> mesh) || mesh.is_empty()) {
        std::cerr << "Le fichier donné n'est pas un fichier off valide." << std::endl;
        return 1;
    }
    //faces = CalculPerimetre(mesh);  //A CHOISIR
    faces = CalculAire(mesh);

    faces_seuillage = seuillageOtsu(mesh,faces); // A CHOISIR
    //faces_seuillage = seuillageSimple(mesh,faces);

    //faces_seuillage = segmentationParCC(mesh,faces_seuillage); //OPTIONNEL

    std::cout<<"nb faces : "<< faces.size() << std::endl;
    sauverSegmentation(filename,mesh,faces_seuillage);
    
    

    return 0;
}