# Rapport du TP N°5 sur la structure octree
## Structure octree
La première étape du TP consistait à partir d'un maillage fourni au format .OFF à générer un arbre appelé octree.

Pour cela nous sommes partis sur la création d'un objet pour representer notre arbre.
Tout les noeuds ayant un même noeud commun dans leurs parents, il sera plus simple dans le programme de manipuler un objet représentant le noeud racine plutôt qu'une liste de noeuds.

Notre objet que nous appellerons Noeud possède les attributs suivant :
+ 6 flottants représentant les coordonnées minimum et maxium sur les 3 axes de la boîte englobante.
+ D'un entier pour la profondeur du noeud
+ D'un entier pour la couleur du noeud. Cet entier servira de graine pour la génération de nombre aléatoire plus tard.
+ D'une liste de noeud fils
+ D'une liste de sommets liés au noeud
+ D'un attribut de classe permettant de connaitre le nombre de noeud généré.

Par bonne pratique, nous avons ajouté à notre classe un constructeur par défaut, un constructeur de copie ainsi que les getters et les setters des différents attributs.

Les méthodes que nous avons dévellopé sont les suivantes:

### ajoutSommet
Cette méthode nous permet d'ajouter un sommet à notre arbre. Pour cela on vérifie que le noeud dans lequel on souhaite ajouter notre sommet ne possède pas de fils et si il est en capacité d'ajouter un sommet (nombre de sommets dans sa liste inférieure à une valeur maximale choisie ou profondeur du noeud égale à la profondeur maximum). Si tel est le cas, le sommet est ajouté à la liste de sommets du noeud. Dans le cas contraire deux situation peuvent arriver :
+ Le noeud possède des noeuds fils.
A l'aide de la méthode touver_noeud on cherche parmis les noeuds fils celui qui est adapté pour recevoir notre sommet puis on appelle recursivement sa méthode ajoutSommet.
+ Le noeud ne possède pas de fils.
Dans ce cas, on vient créer 8 fils à l'aide de la méthode creerFils. On déplace les sommets de la liste dans les nouveaux noeud fils avant de la vider. Enfin on recherche le noeud adéquat dans les fils pour lancer recursivement sa méthode ajoutSommet.
### creer_fils
Cette méthode calcul les coordonnées du milieu de notre intervalle [ xmin ; xmax ] et fait de même pour les 2 autres axes. Puis 8 nouveaux Noeuds sont construits (avec les sommets du noeud de base et le point du milieu on obtient 8 nouvelles boîtes englobantes)et ajoutés dans la liste de noeuds fils du noeud parent. L'ordre d'ajout des noeuds à une importance, elle nous sert dans la méthode trouver_noeud à retrouver l'index dans la liste de noeud fils d'un noeud pour ajouter un sommet.
### trouver_noeud
Cette méthode a pour objectif, à partir d'une liste de noeud fils, de retrouver le noeud dont la boîte englobante contient le sommet donné en paramètre. Pour cela on calcul comme dans la méthode creer_fils  les coordonnées milieu pour chaque axe de la boite englobante. En fonction des coordonées du point passé en paramètre si elles sont inférieures à la coordonnée milieu de l'axe correspondant, on attribu la valeur 0 (1 sinon). Puis à l'aide de ces valeurs on peut facilement retrouver l'index du noeud adapté dans la liste de noeud fils à l'aide de l'équation :

index = 4 * x + 2 * y + z

À l'aide de cet index on retourne le bon noeud dans la liste des fils. 

## Colorisation de maillages
La colorisation du maillage se fait en enregistrant le fichier au format .COFF. Ce format fonctionne de la même façon que le format .OFF à la différence que la déclaration des sommets possède en plus 3 composantes de couleurs (rouge, vert, bleu). 
Pour coloriser notre maillage nous avons besoin d'une structure map permettant d'associé les sommets aux noeuds auquel ils appartiennent. Dans l'explication de la classe octree nous avons parlé d'un attribut entier qui sert à la fois pour la couleur et pour identifier les noeuds, c'est ici qu'il va nous servir. On déclare une variable globale de type map composée de paires de sommets et d'entiers. Cette map est initialisé dans une fonction appelée initMap, où chaque sommet y est inséré avec une valeur entière nulle. Lors de la création d'un octree, chaque fois qu'un sommet est inséré dans la liste de sommets d'un noeud, on met à jour cette map en associant le sommet à l'attribut couleur du noeud.

Une fois la map mise à jour il suffit lors de l'écriture dans le fichier d'utiliser la couleur du sommet comme graine puis de générer trois nombres aléatoires modulo 255 et des les écrire. 

## Simplification de maillages
La simplification de maillage peut se décomposer en plusieures étapes. Tout d'abord, grâce à la répartition effectué par l'octree sur le maillage, on va définir de nouveaux sommets permettant la simplification. Chaque noeud de l'octree contient (généralement) différents sommets qu'il faudra fusionner pour obtenir un sommet unique représentant ce noeud. Une fois les nouveaux sommets obtenus, il faut construire des faces de façon cohérente avec le maillage de départ. Enfin, on vient créer un nouveau fichier .OFF dans lequel on stocke les sommets et les faces créés par la simplification.

### FusionPts
Cette fonction permet de créer les nouveaux sommets pour la simplification. Nous avons choisi d'utiliser ici le barycentre des sommets composant un noeud. Cette méthode a l'avantage d'être simple à mettre en place et de donner des résultats corrects, en prenant en compte les valeurs aberrantes potentielles et en restant assez fidèle à la position des sommets de départ. 

Pour faciliter l'écriture dans le fichier .OFF, nous allons également en profiter pour associer chaque noeud à un indice qui le qualifie (initialisé à 0 et incrémenté ensuite). Cet indice correspond à la position où sera défini le sommet simplifié dans le fichier .OFF Cette position est utilisé pour créer les faces et relier les sommets dans le fichier, il est donc très important qu'il soit cohérent. L'indice du noeud est ensuite associé à tous les sommets qui le compose dans un map.

Dans cette fonction, on parcourt les différents noeuds de l'octree : 
* Tant qu'un noeud a des fils, il ne contient pas de sommets, on peut donc appeler récurcivement FusionPts sur les 8 noeuds fils.
* Si un noeud n'a pas de fils, mais qu'il ne contient pas de sommets non plus, alors on ne le prend pas en compte. Cette condition est importante car si le noeud est ajouté à la map et lui associe un indice, le noeud sera écrit avec aucune valeur dans le fichier .OFF ce qui le rendra inutilisable. 
* Le noeud contient au moins 1 sommet :
    + Dans ce cas, on va faire le barycentre des sommets qu'il contient. On crée un nouveau sommet (classe Fusionsommets qui contient 3 coordonnées x, y, z). On parcourt la liste de sommets du noeud avec un itérateur pour calculer le barycentre (xmoy = somme des x de tous les sommets / nombre de sommets). 
    + Pendant ce parcours, on associe chaque sommet avec un indice qui correspond au noeud dans lequel il est. Les sommets d'un même noeud ont donc le même indice.
    + À la fin du parcours, on incrémente l'indice (pour identifier le noeud), et on ajoute le sommet créé à la liste.

### sauverSimplification
Cette fonction a deux rôles : associer les sommets pour créer les faces du maillage simplifié, et créer un nouveau fichier .OFF contenant le maillage simplifié.
Après avoir récupéré les sommets simplifiés ainsi qu'une map associant sommet et indice du noeud grâce à la fonction FusionPts, on commence à créer le fichier .OFF. Nous n'avons pas réussi à ajouter du texte en début de fichier sans écrire par dessus le contenu existant, nous commencons donc par définir l'entête, que nous réécririons plus tard. Une fois le fichier ouvert et l'entête prévue, nous allons écrire dans le fichier la liste des sommets. Pour cette étape, il nous suffit de parcourir la liste des sommets simplifiés créée par FusionPts et d'écrire "x y z" dans le fichier. Chaque saut de ligne correspond à un nouveau sommet. Une fois tous les sommets entrés, c'est au tours des faces d'êtres écrites. 

Nous allons définir les faces à créer en même temps que nous les écrivons. Pour cela, nous allons parcourir toutes les faces du maillage de base. Pour chaque face nous allons parcourir les sommets qui la compose grâce à un circulator et récupérer l'indice de ses sommets en utilisant la map créée dans FusionPts (qui associe un sommet à l'indice de son noeud). 
Pour éviter des problèmes (si par exemple deux sommet d'une face appartiennent au même deux, et vont donc se simplifier) on choisit de ne créer un face que lorsque les sommets composant la face sont tous dans des noeuds différents (indices différents). 
Pour créer une face dans notre fichier, on ajoute une ligne commençant par le nombre de sommets reliés, et contenant l'indice des sommets qui la compose (le sommet écrit en 1er dans le .OFF a l'indice 0, le 2ème -> 1 etc.). 
Ces indices sont les mêmes que ceux que l'on a stocké dans notre map et associé à chaque sommet, nous pouvons donc les utiliser facilement. En effet chaque noeud s'est vu associer un indice, et les sommets de ce noeud ont tous ce même indice. Quand nous avons écrit dans le fichier, le sommet correspondant à un noeud a été écrit dans le même ordre que quand il a reçu son indice. Donc l'indice associé aux anciens sommets par la map correspond à la ligne sur laquelle on pourra trouver le sommet créé à partir du noeud dans lequel ils étaient.

À chaque face créée, on incrémente également le nombre de face de manière à pouvoir l'écrire dans l'entête ensuite. Un fois toutes les sommets simplifiés reliés, il ne reste plus qu'à réécrire l'entête pour préciser le nombre de sommets, le nombre de faces et le nombre d'arêtes (nb de faces *3). Après certains tests, nous avons constaté que le nombre d'arêtes n'était pas obligatoire mais nous avons choisi de le laisser pour plus de clarté.

## Test fonctionnement
Lors de la programmation nous avons essayé notre algorithme sur le fichier bunny.off. Avec ses nombreux sommets et faces nous pouvions nous rendre compte de la performance de l'algorithme. La forme est aussi suffisament peut commune pour généralisé le bon fonctionnement de l'algorithme (certains bugs pouvant passer inaperçu sur des formes simples).

Nous avons testé notre programme à deux etapes :
+ Après avoir fini la colorisation de maillage (il était compliqué avant cet étape de visualiser l'arbre). Cela nous a permi aussi de directement penser notre classe Noeud avec de la couleur.

+ Après l'étape de simplification

### Essais de l'octree et de la colorisation
Tout d'abord nous avons essayé notre programme avec une profondeur de 2 et un nombre de sommets maximum par noeud de 10. Nous nous attendons à observer 8 zones colorisé distinct (la profondeur étant bloquante ici).

<img src="https://gitlab.com/tony.vernet98/geometrie-algorithmique/-/raw/4eeb538f47f6671d14eb79c4802b271a86b67dbe/documentation/Images/bunny_p2_s10.png"  width="500" height="500">

Les 8 zones sont bien présentes sur le maillage finale. Nous avons ensuite essayé avec une profondeur plus importante(3) :

<img src="https://gitlab.com/tony.vernet98/geometrie-algorithmique/-/raw/4eeb538f47f6671d14eb79c4802b271a86b67dbe/documentation/Images/bunny_p3_s10.png"  width="500" height="500">

On constate que le nombre de couleur à été multiplié par 8 ce qui correspond à une profondeur de 3. Avec un si grand nombre de sommets et une profondeur si faible nous considerons que chaque noeud de l'octree possède au moins un sommet ce qui nous donne 8^profondeur couleurs différentes.

### Essais de la simplifcation
Pour la phase de simplification, nous savions avec le sujet mais aussi par intuition qu'une profondeur trop faible donnerait une simplifaction très éloignée du modèle original. Pour commencer nous avons fait le test avec une profondeur de 5 et un nombre de sommets de 10:

<img src="https://gitlab.com/tony.vernet98/geometrie-algorithmique/-/raw/4eeb538f47f6671d14eb79c4802b271a86b67dbe/documentation/Images/Simpl_bunny_p5_s10.png"  width="500" height="500">

La simplification semble fonctionner correctement et le nombre de sommets corresponds au nombre de noeuds possédants au moins un sommet dans le graphe (ici 890)
Nous avons essayé avec une profondeur plus importante pour observer si la simplification se rapprochait du modèle original tout en ayant un nombre de sommets plus important.

<img src="https://gitlab.com/tony.vernet98/geometrie-algorithmique/-/raw/4eeb538f47f6671d14eb79c4802b271a86b67dbe/documentation/Images/Simpl_bunny_p30_s10.png"  width="500" height="500">

On constate une plus grande precision qu'avec l'image précédente. Enfin, pour nous assuré de la cohérence de la simplifaction, nous avont utilisé la simplification sur un octree avec une très grande profondeur et un nombre de sommets maximum par noeud de 1. Nous nous attendons à obtenir le même modèle de lapin que l'originale puisque chaque sommets est dans un noueud différent, il n'y a donc aucune fusion.

<img src="https://gitlab.com/tony.vernet98/geometrie-algorithmique/-/raw/4eeb538f47f6671d14eb79c4802b271a86b67dbe/documentation/Images/Simpl_bunny_p800_s1.png"  width="500" height="500">

Comme dans nos attentes le résultat et bien celui du fichier originale avec le même nombre de sommets et de faces.
Nous pouvons en conclure que l'algorithme fonctionne.

Pour terminer nous avons utilisé un autre jeu de données sur notre programme ici venus.off :

Profondeur 2 , sommets max 10

<img src="https://gitlab.com/tony.vernet98/geometrie-algorithmique/-/raw/a2d86f5b95a05a87e8fdf06a14fb7d6f10467ae7/documentation/Images/venus_p2_s10.png"  width="400" height="600">

Profondeur 10 , sommets max 10

<img src="https://gitlab.com/tony.vernet98/geometrie-algorithmique/-/raw/a2d86f5b95a05a87e8fdf06a14fb7d6f10467ae7/documentation/Images/Simpl_venus_p10_s10.png"  width="400" height="600">

## Discussion
La colorisation et la simplification mises en place dans ce TP fonctionnent correctement et donnent de bons résultats. La fusion des différents sommets d'un noeud grâce au barycentre fournit un résultat proche de l'original. Nous aurions également pu se contenter de choisir un sommet au hasard parmi tous les sommets du noeud, ou de le choisir selon des critères plus précis. 
Pour ce qui concerne la création des faces, nous n'avons envisagé que cette solution, qui permet d'éviter les problèmes mis en lumière dans l'énoncé du TP. Cela permet de reconstituer à l'identique le maillage s'il n'y a qu'un sommet par noeud, et simplifie assez bien avec un nombre de sommets réduit. 
Notre programme a cependant plusieurs limites :
+ Les maillages manipulés pour la simplification doivent êtres composés de faces à 3 sommets. Il aurait été possible de prendre en compte les faces composées de 4 sommets et plus en regardant le nombre de sommets composant chaque face et en implémentant une condition lors de l'écriture des nouvelles faces (regarder l'indice des 4 sommets et voir si ils sont dans le même noeud). Cependant, comme la majorité des maillages sont composés de faces à 3 sommets, nous n'avons pas souhaité alourdir le code.
+ Certaines faces ne sont pas orientés correctement lors de la simplification. Cette orientation étant faite en fonciton du parcours des sommets, nous ne savons pas comment résoudre ce problème, qui reste cependant très peu fréquent.
